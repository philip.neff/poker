package poker.version_graphics.view;

import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import poker.version_graphics.PokerGame;
import poker.version_graphics.controller.PokerGameController;
import poker.version_graphics.model.PokerGameModel;

public class StartPage extends VBox {
	
	PokerGameModel model;
	PokerGameView view;
	PokerGameController controller;
	
	private Label numberOfPlayers;
	private RadioButton twoPlayers, threePlayers, fourPlayers;
	private Button startGamebtn;
	

	public StartPage(Stage stage) {
		
		
		this.numberOfPlayers = new Label("Number of players?");
		this.twoPlayers = new RadioButton("2 Players");
		this.threePlayers = new RadioButton("3 Players");
		this.fourPlayers = new RadioButton("4 Players");
		
		
		ToggleGroup tg = new ToggleGroup();
		this.twoPlayers.setToggleGroup(tg);
		this.threePlayers.setToggleGroup(tg);
		this.fourPlayers.setToggleGroup(tg);
		this.twoPlayers.setSelected(true);
		
		this.startGamebtn = new Button("Start Game!");
		this.startGamebtn.setOnAction(e -> startGame(stage));
		
		this.getChildren().addAll(this.numberOfPlayers, this.twoPlayers, this.threePlayers, this.fourPlayers, this.startGamebtn);
		Scene scene = new Scene(this, 800, 400);//was 250, 400
		scene.getStylesheets().add(getClass().getResource("poker.css").toExternalForm());
       	this.getStyleClass().add("startPage");
    	stage.setScene(scene);
    	stage.show();
		

		this.setAlignment(Pos.CENTER);
		this.setSpacing(20);
	}
	
	public void startGame(Stage stage) {
		
		if (twoPlayers.isSelected())
			PokerGame.NUM_PLAYERS = 2;
			
		if (threePlayers.isSelected())
			PokerGame.NUM_PLAYERS = 3;
		
		if (fourPlayers.isSelected())
			PokerGame.NUM_PLAYERS = 4;
		
		model = new PokerGameModel();
    	view = new PokerGameView(stage, model);
    	controller = new PokerGameController(model, view);
    	
	}
	
}
