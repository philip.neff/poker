package poker.version_graphics.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import poker.version_graphics.model.Card;
import poker.version_graphics.model.HandType;
import poker.version_graphics.model.Player;

public class PlayerPane extends VBox {
    private Label lblName = new Label();
    private HBox hboxCards = new HBox();
    private Label lblEvaluation = new Label("--");
    private ImageView imagePlayerPicture = new ImageView();
    private Button btnAddPicture = new Button();
    private BorderPane imageViewWrapper; 
    private Label wins = new Label("Wins 0");
    
    // Link to player object
    private Player player;
    
    public PlayerPane() {
        super(); // Always call super-constructor first !!
        this.getStyleClass().add("player"); // CSS style class

        this.btnAddPicture.setText("Add Player Picture");
        
        this.imagePlayerPicture.setFitWidth(100);
    	this.imagePlayerPicture.setFitHeight(100);
    	
    	
    	
    	
    	//Create Borderpane with Image-View to Style CSS
        imageViewWrapper = new BorderPane(this.imagePlayerPicture);
        imageViewWrapper.getStyleClass().add("image-view");
    	
        // Add child nodes
        this.getChildren().addAll(imageViewWrapper, btnAddPicture,lblName, hboxCards, lblEvaluation, wins);
        
        // Add CardLabels for the cards
        for (int i = 0; i < 5; i++) {
            Label lblCard = new CardLabel();
            hboxCards.getChildren().add(lblCard);
        }
    }
    
    public void addPicture() {
    	try {
	    	Image image = new Image(new FileInputStream(player.getPicturePath()));
	    	this.imagePlayerPicture.setImage(image);
	    	//Make Btn invisble so User cann add only one picture
	    	btnAddPicture.setVisible(false);
    	}
    	catch(FileNotFoundException e){
    		
    	}
    }
    
    public void setPlayer(Player player) {
    	this.player = player;
    	this.btnAddPicture.setId(player.getPlayerName());
    	updatePlayerDisplay(); // Immediately display the player information
    }
    
    public void updatePlayerDisplay() {
    	lblName.setText(player.getPlayerName());
    	for (int i = 0; i < Player.HAND_SIZE; i++) {
    		Card card = null;
    		if (player.getCards().size() > i) card = player.getCards().get(i);
    		CardLabel cl = (CardLabel) hboxCards.getChildren().get(i);
    		cl.setCard(card);
    		HandType evaluation = player.evaluateHand();
    		if (evaluation != null)
    			lblEvaluation.setText(evaluation.toString());
    		else
    			lblEvaluation.setText("--");
    	}
    }
    public Button getAddPictureButton() {
    	return this.btnAddPicture;
	}
    
    public void ResetWinColor() {
    	wins.setStyle("-fx-text-fill: White");
    }
    //Updates the win Counter to Actuatl Win numbers + Colorize
    public void UpdateWinCounter() {
    	wins.setStyle("-fx-text-fill: Blue");
    	wins.setText("Wins " + player.getWinCount());
    }
}
