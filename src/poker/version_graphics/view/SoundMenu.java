package poker.version_graphics.view;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import poker.version_graphics.model.Sound;

public class SoundMenu extends HBox{
	
	private Slider slider = new Slider();
	private Label label = new Label("Volume ");
	private double volume = 0.5;
	
	

	
	public SoundMenu() {
		super();
        this.getStyleClass().add("soundMenu"); // CSS style class
        
       
        	slider.setMin(0);
        	slider.setMax(1);
        	slider.setValue(0.5);
        	slider.setMaxWidth(200);
     
        	slider.setShowTickLabels(false);
        	slider.setShowTickMarks(false);
     
        	slider.setBlockIncrement(2);
        	slider.setOnMouseDragged(sliderHandler);
        	slider.setOnMouseClicked(sliderHandler);
      
       
    	
    	this.getChildren().addAll(label, slider);

        HBox.setHgrow(slider, Priority.ALWAYS); // Use region to absorb resizing
        this.setId("soundMenu"); // Unique ID in the CSS		
	}
	
	 //aktion
	EventHandler <MouseEvent> sliderHandler = e ->{
		volume = slider.getValue();
		Sound.setVolume(volume);
		//System.out.println(volume); //Überprüfung Volumenslider
		
	};



	public double getVolume() {
		return volume;
	}
}
