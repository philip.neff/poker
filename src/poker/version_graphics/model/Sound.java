package poker.version_graphics.model;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import poker.version_graphics.view.SoundMenu;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

public class Sound {
		public static final String SoundDirPath = "src/poker/sounds/";
		private static final String ShuffleFileName = "ShuffleSound.wav";
		private static final String DealFileName = "DealSound.wav";
		private static final String BackgroundMusicName = "BackgroundSound.wav";
		private static MediaPlayer mediaPlayer;
		//Test
		
		public static void playDealSound() {
			String musicFile = SoundDirPath + DealFileName;
			playSound(musicFile);
		}
		
		public static void playShuffleSound() {
			String musicFile = SoundDirPath + ShuffleFileName;
			playSound(musicFile);
		}
		
		public static void playBackgroundMusic() {
			String musicFile = SoundDirPath + BackgroundMusicName;
			playBgSound(musicFile);
		}
		
		public static void playSound(String path) {
			Media sound = new Media(new File(path).toURI().toString());
			MediaPlayer mediaPlayer = new MediaPlayer(sound);
			mediaPlayer.play();			
		}
		
		public static void playBgSound(String path) {
			try {
			
				Media data = new Media(new File(path).toURI().toString());
				SoundMenu soundMenu = new SoundMenu();
				mediaPlayer = new MediaPlayer(data);
				mediaPlayer.setVolume(soundMenu.getVolume());
				mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
				mediaPlayer.play();
				
				
				//Anderer, nicht funktionierender Ansatz mit Loop:
				//AudioData data = new AudioStream(new FileInputStream(path)).getData();
				//ContinuousAudioDataStream sound = new ContinuousAudioDataStream(data);
				//AudioPlayer.player.start(sound);
				
			
			}
			catch (Exception e)
			{
				System.out.println("Error");
			}
		}
		public static void setVolume(double value) {
			mediaPlayer.setVolume(value);
		}
}
