package poker.version_graphics.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import Helpers.RankComparator;
import Helpers.RankOrdinalComperator;
import poker.version_graphics.model.Card.Rank;
import poker.version_graphics.model.Card.Suit;

/**
 * @author philip Neff
 *
 */
public enum HandType {
    HighCard, OnePair, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush, RoyalFlush;
    
    /**
     * Determine the value of this hand. Note that this does not
     * account for any tie-breaking
     */
    public static HandType evaluateHand(ArrayList<Card> cards) {
        HandType currentEval = HighCard;
        
        if (isOnePair(cards)) currentEval = OnePair;
        if (isTwoPair(cards)) currentEval = TwoPair;
        if (isThreeOfAKind(cards)) currentEval = ThreeOfAKind;
        if (isStraight(cards)) currentEval = Straight;
        if (isFlush(cards)) currentEval = Flush;
        if (isFullHouse(cards)) currentEval = FullHouse;
        if (isFourOfAKind(cards)) currentEval = FourOfAKind;
        if (isStraightFlush(cards)) currentEval = StraightFlush;
        if (isStraightFlush(cards)) currentEval = RoyalFlush;
        
        return currentEval;
    }
    
    public static Card getHighCard(ArrayList<Card> cards) {
        //Sort List by Rank-Ordianl
    	Collections.sort(cards, (c1,c2) -> Double.compare(c1.getRank(), c2.getRank()));
    	cards.sort((c1, c2) -> Double.compare(c1.getRank().ordinal(), c2.getRank().ordinal()) );
    	
       	Collections.sort(cards, new RankOrdinalComperator());
       	return cards.get(cards.size() - 1);
    }
    
    //Region Handtype-Checkers - Philip Neff
    public static boolean isOnePair(ArrayList<Card> cards) {
        boolean found = false;
        for (int i = 0; i < cards.size() - 1 && !found; i++) {
            for (int j = i+1; j < cards.size() && !found; j++) {
                if (cards.get(i).getRank() == cards.get(j).getRank()) found = true;
            }
        }
        return found;
    }
    
    public static boolean isTwoPair(ArrayList<Card> cards) {
        // Clone the cards, because we will be altering the list
        ArrayList<Card> clonedCards = (ArrayList<Card>) cards.clone();

        // Find the first pair; if found, remove the cards from the list
        boolean firstPairFound = false;
        for (int i = 0; i < clonedCards.size() - 1 && !firstPairFound; i++) {
            for (int j = i+1; j < clonedCards.size() && !firstPairFound; j++) {
                if (clonedCards.get(i).getRank() == clonedCards.get(j).getRank()) {
                    firstPairFound = true;
                    clonedCards.remove(j);  // Remove the later card
                    clonedCards.remove(i);  // Before the earlier one
                }
            }
        }
        // If a first pair was found, see if there is a second pair
        return firstPairFound && isOnePair(clonedCards);
    }
   
    
    /**
    * @author Philip Neff
    */
    public static boolean isThreeOfAKind(ArrayList<Card> cards) {
       	Rank newRank, oldRank= null;
    	int rankCounter = 1;
    	//Sort cards by Rank
    	Collections.sort(cards, new RankComparator());
    	for (Card c : cards) {
    		newRank = c.getRank();
    		if (newRank == oldRank) {
    			rankCounter++;
    		}
    		else {
    			rankCounter = 1;
    		}
    		if (rankCounter == 3) {
    			return true;
    		}
    		oldRank = newRank;
    		
    	}
        return false;
    }
    
    /**
     * @author Philip Neff
     */
    public static boolean isStraight(ArrayList<Card> cards) {
        int newOrdinal = 0, oldOrdinal = 0;
        
        //Sort List by Rank-Ordianl
    	Collections.sort(cards, new RankOrdinalComperator());
    	//Set oldOrdianl to firstValue - 1 to make sure, that for if-Condition is true
    	oldOrdinal = cards.get(0).getRank().ordinal() - 1;
    	//Sum the Ordinals - the lowest Ordinal --> It has to be 10 to be a Straight
    	for (Card c : cards) {
    		newOrdinal =c.getRank().ordinal();
    		if (newOrdinal != oldOrdinal +1) {
    			return false;
    		}
    		oldOrdinal = newOrdinal;
    	}
    	return true;
       }
    
    /**
     * @author Philip Neff
     */
    public static boolean isFlush(ArrayList<Card> cards) {
    	Suit newSuit, oldSuit= null;
    	for (Card c : cards) {
    		newSuit = c.getSuit();
    		if (newSuit != oldSuit && oldSuit != null) {
    			return false;
    		}
    		oldSuit = newSuit;
    	}
    	return true;
    }
    
    /**
     * @author Philip Neff
     */
    public static boolean isFullHouse(ArrayList<Card> cards) {
    	if (isTwoPair(cards) && isThreeOfAKind(cards) && !isFourOfAKind(cards)){
    		return true;
    	}
    	else {
    		return false;
    	}
    	
    }
    
    /**
     * @author Philip Neff
     */
    public static boolean isFourOfAKind(ArrayList<Card> cards) {
    	Rank newRank, oldRank= null;
    	int rankCounter = 1;
    	//Sort cards by Rank
    	Collections.sort(cards, new RankComparator());
    	//Loop through sorted List and check newCard with oldCard (Card before actual one)
    	for (Card c : cards) {
    		newRank = c.getRank();
    		if (newRank == oldRank) {
    			rankCounter++;
    		}
    		else {
    			rankCounter = 1;
    		}
    		if (rankCounter == 4) {
    			return true;
    		}
    		oldRank = newRank;
    	}
        return false;
    }
    
    /**
     * @author Philip Neff
     */
    public static boolean isStraightFlush(ArrayList<Card> cards) {
        return isStraight(cards) && isFlush(cards);
    }
    
    /**
     * @author Philip Neff
     */
    public static boolean isRoyalFlush(ArrayList<Card> cards) {
        if( isStraightFlush(cards)) {
        	int ordinalSum = 0;
        	//Sum the Ordinals --> Has to be 50 to be a Royal Flush (Ten = 8, Jack = 9, Queen = 10, King = 11, Ace = 12)
         	for (Card c : cards) {
         		ordinalSum += c.getRank().ordinal();
         	}
         	return (ordinalSum == 50);
        }
        else {
        	return false;
        }
    	
    }
    //Endregion Handtype-Checker - Philip Neff
    
}
