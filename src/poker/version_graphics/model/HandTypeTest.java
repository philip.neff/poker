package poker.version_graphics.model;

import static org.junit.Assert.*;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class HandTypeTest {
	// We define the hands using abbreviations. The code at the bottom
	// of this class can translate one of these strings into a card.
	//
	// Another method takes a set of five cards, and translates the whole hand
	//
	// Yet another method does this for a whole set of hands
	private static String[][] highCards = {
			{ "2S", "9C", "3H", "5D", "7H" },
			{ "7S", "5C", "AH", "JD", "6H" },
			{ "2S", "3S", "4S", "5S", "7S" },
			{ "AS", "KC", "QH", "JD", "TH" }
			};
	
	private static String[][] pairs = {
			{ "2S", "2C", "3H", "5D", "7H" },
			{ "2S", "AC", "3H", "5D", "AH" },
			{ "3S", "2C", "3H", "KD", "QH" },
			{ "9S", "2C", "2H", "5D", "7H" }
			};

	private static String[][] twoPairs = {
			{ "2S", "2C", "7H", "5D", "7H" },
			{ "2S", "AC", "5H", "5D", "AH" },
			{ "3S", "2C", "3H", "2D", "QH" },
			{ "9S", "2C", "2H", "5D", "5H" }
			};
	private static String[][] threeOfAKind = {
			{ "2S", "2C", "2H", "5D", "7H" },
			{ "5D", "5S", "5H", "4D", "AH" },
			{ "3S", "3C", "3H", "2D", "QH" },
			{ "9S", "9C", "9H", "5D", "5H" }
			};
	
	private static String[][] fullHouse = {
			{ "TC", "TS", "TD", "KC", "KS" },
			{ "2S", "2C", "AD", "AS", "AH" },
			{ "3S", "3D", "QC", "QH", "QD" },
			{ "9S", "9H", "5S", "5D", "5H" }
			};
	
	private static String[][] fourOfAKind= {
			{ "2S", "2C", "2D", "2H", "7H" },
			{ "2S", "AC", "AD", "AS", "AH" },
			{ "3S", "QS", "QC", "QH", "QD" },
			{ "9S", "5C", "5S", "5D", "5H" }
			};
	
	private static String[][] flush= {
			{ "2D", "3D", "4D", "5D", "7D" },
			{ "2H", "KH", "QH", "3H", "AH" },
			{ "3S", "QS", "KS", "AS", "9S" },
			{ "9C", "5C", "6C", "7C", "AC" }
			};
	
	private static String[][] straight = {
			{ "2D", "3C", "4D", "5S", "6D" },
			{ "6H", "7H", "TD", "9H", "8H" },
			{ "TD", "JS", "QC", "KS", "AS" },
			{ "QC" ,"9C", "TD", "JS", "KC" }
			};
	
	private static String[][] straightFlush = {
			{ "2D", "3D", "4D", "5D", "6D" },
			{ "6H", "7H", "TH", "9H", "8H" },
			{ "9S", "TS", "JS", "QS", "KS" },
			{ "QC" ,"9C", "TC", "JC", "KC" }
			};
	
	private static String[][] royalFlush = {
			{ "TC", "JC", "QC", "KC", "AC" },
			{ "TD", "JD", "QD", "KD", "AD" },
			{ "TH", "JH", "QH", "KH", "AH" },
			{ "TS", "JS", "QS", "KS", "AS" },
			};
	

	
	// This is where we store the translated hands
	ArrayList<ArrayList<Card>> highCardHands;
	ArrayList<ArrayList<Card>> pairHands;
	ArrayList<ArrayList<Card>> twoPairHands;
	ArrayList<ArrayList<Card>> threeOfAKindHands;
	ArrayList<ArrayList<Card>> fourOfAKindHands;
	ArrayList<ArrayList<Card>> flushHands;
	ArrayList<ArrayList<Card>> straightHands;
	ArrayList<ArrayList<Card>> straightFlushHands;
	ArrayList<ArrayList<Card>> royalFlushHands;
	ArrayList<ArrayList<Card>> fullHouseHands;
	
	
	/**
	 * The makeHands method is called before each test method,
	 * and prepares the translated hands. We recreate these for
	 * each test method, in case the test method damages the data.
	 */
	@Before
	public void makeHands() {
		this.highCardHands = makeHands(highCards);
		this.pairHands = makeHands(pairs);
		this.twoPairHands = makeHands(twoPairs);
		this.threeOfAKindHands = makeHands(threeOfAKind);
		this.fourOfAKindHands = makeHands(fourOfAKind);
		this.flushHands = makeHands(flush);
		this.straightHands = makeHands(straight);
		this.straightFlushHands = makeHands(straightFlush);
		this.royalFlushHands= makeHands(royalFlush);
		this.fullHouseHands = makeHands(fullHouse);
	}

	/**
	 * This is a test method for the isOnePair method in HandType.
	 * We expect all HighCard hands to be false, all OnePair hands to
	 * be true, all TwoPair hands to be true, etc.
	 */
	@Test
	public void testIsOnePair() {
		for (ArrayList<Card> hand : highCardHands) {
			assertFalse(HandType.isOnePair(hand));
		}
		for (ArrayList<Card> hand : pairHands) {
			assertTrue(HandType.isOnePair(hand));
		}
		for (ArrayList<Card> hand : twoPairHands) {
			assertTrue(HandType.isOnePair(hand)); // Two-pair contains a pair
		}
	}

	/**
	 * This is the test method for the isTwoPair in HandType.
	 *  
	 */
	@Test
	public void testIsTwoPair() {
		for (ArrayList<Card> hand : highCardHands) {
			assertFalse(HandType.isTwoPair(hand));
		}
		for (ArrayList<Card> hand : pairHands) {
			assertFalse(HandType.isTwoPair(hand));
		}
		for (ArrayList<Card> hand : twoPairHands) {
			assertTrue(HandType.isTwoPair(hand));
		}
	}
	/**
	 * This is the test method for the isThreeOfAKind in HandType.
	 * @author philip
	 */
	@Test
	public void testIsThreeOfAKind() {
		for (ArrayList<Card> hand : highCardHands) {
			assertFalse(HandType.isThreeOfAKind(hand));
		}
		for (ArrayList<Card> hand : pairHands) {
			assertFalse(HandType.isThreeOfAKind(hand));//ThreeOfAKind is also a Pair
		}
		for (ArrayList<Card> hand : twoPairHands) {
			assertFalse(HandType.isThreeOfAKind(hand));
		}
		for (ArrayList<Card> hand : threeOfAKindHands) {
			assertTrue(HandType.isThreeOfAKind(hand));
		}
		for (ArrayList<Card> hand : fourOfAKindHands) {
			assertTrue(HandType.isThreeOfAKind(hand));//fourOfAKind is also ThreeOfAKind
		}
	}
	
	/**
	 * This is the test method for the isFourOfAKind in HandType.
	 * @author philip
	 */
	@Test
	public void testIsFourOfAKind() {
		for (ArrayList<Card> hand : highCardHands) {
			assertFalse(HandType.isFourOfAKind(hand));
		}
		for (ArrayList<Card> hand : pairHands) {
			assertFalse(HandType.isFourOfAKind(hand));//ThreeOfAKind is also a Pair
		}
		for (ArrayList<Card> hand : twoPairHands) {
			assertFalse(HandType.isFourOfAKind(hand));
		}
		for (ArrayList<Card> hand : threeOfAKindHands) {
			assertFalse(HandType.isFourOfAKind(hand));
		}
		for (ArrayList<Card> hand : fourOfAKindHands) {
			assertTrue(HandType.isFourOfAKind(hand));
		}
	}
	/**
	 * This is the test method for the isFourOfAKind in HandType.
	 * @author philip
	 */
	@Test
	public void testFlush() {
		for (ArrayList<Card> hand : flushHands) {
			assertTrue(HandType.isFlush(hand));
		}
	}
	
	/**
	 * This is the test method for the isFourOfAKind in HandType.
	 * @author philip
	 */
	@Test
	public void testStraight() {
		for (ArrayList<Card> hand : straightHands) {
			assertTrue(HandType.isStraight(hand));
		}
	}
	
	/**
	 * This is the test method for the isFourOfAKind in HandType.
	 * @author philip
	 */
	@Test
	public void testStraightFlush() {
		for (ArrayList<Card> hand : straightHands) {
			assertFalse(HandType.isStraightFlush(hand)); 
		}
		for (ArrayList<Card> hand : straightFlushHands) {
			assertTrue(HandType.isStraightFlush(hand)); 
		}
		for (ArrayList<Card> hand : royalFlushHands) {
			assertTrue(HandType.isStraightFlush(hand)); //RoyalFlush is also a StraightFlash
		}
	}
	
	/**
	 * This is the test method for the isFourOfAKind in HandType.
	 * @author philip
	 */
	@Test
	public void testRoyalFlush() {
		for (ArrayList<Card> hand : straightHands) {
			assertFalse(HandType.isRoyalFlush(hand));
		}
		for (ArrayList<Card> hand : straightFlushHands) {
			assertFalse(HandType.isRoyalFlush(hand));
		}
		for (ArrayList<Card> hand : royalFlushHands) {
			assertTrue(HandType.isRoyalFlush(hand));
		}
	}
	
	/**
	 * This is the test method for the isFullHouse in HandType.
	 * @author philip
	 */
	@Test
	public void testFullHouse() {
		for (ArrayList<Card> hand : fullHouseHands) {
			assertTrue(HandType.isOnePair(hand)); 		//FullHOuse is also OnePair
		}
		for (ArrayList<Card> hand : fullHouseHands) {
			assertTrue(HandType.isTwoPair(hand));		//FullHouse is also TwoPair
		}
		for (ArrayList<Card> hand : fullHouseHands) {
			assertTrue(HandType.isThreeOfAKind(hand)); //FullHouse is also ThreeOfAKind
		}
		for (ArrayList<Card> hand : fullHouseHands) {
			assertTrue(HandType.isFullHouse(hand));
		}
	}
	
	/**
	 * Make an ArrayList of hands from an array of string-arrays
	 */
	private ArrayList<ArrayList<Card>> makeHands(String[][] handsIn) {
		ArrayList<ArrayList<Card>> handsOut = new ArrayList<>();
		for (String[] hand : handsIn) {
			handsOut.add(makeHand(hand));
		}
		return handsOut;
	}
	
	/**
	 * Make a hand (ArrayList<Card>) from an array of 5 strings
	 */
	private ArrayList<Card> makeHand(String[] inStrings) {
		ArrayList<Card> hand = new ArrayList<>();
		for (String in : inStrings) {
			hand.add(makeCard(in));
		}
		return hand;
	}
	
	/**
	 * Create a card from a 2-character String.
	 * First character is the rank (2-9, T, J, Q, K, A) 
	 * Second character is the suit (C, D, H, S)
	 * 
	 * No validation or error handling!
	 */
	private Card makeCard(String in) {
		char r = in.charAt(0);
		Card.Rank rank = null;
		if (r <= '9') rank = Card.Rank.values()[r-'0' - 2];
		else if (r == 'T') rank = Card.Rank.Ten;
		else if (r == 'J') rank = Card.Rank.Jack;
		else if (r == 'Q') rank = Card.Rank.Queen;
		else if (r == 'K') rank = Card.Rank.King;
		else if (r == 'A') rank = Card.Rank.Ace;
		
		char s = in.charAt(1);
		Card.Suit suit = null;
		if (s == 'C') suit = Card.Suit.Clubs;
		if (s == 'D') suit = Card.Suit.Diamonds;
		if (s == 'H') suit = Card.Suit.Hearts;
		if (s == 'S') suit = Card.Suit.Spades;

		return new Card(suit, rank);
	}
}
