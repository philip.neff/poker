

import java.util.ArrayList;

import javax.swing.JFileChooser;

import Helpers.WinnerEvaluator;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import poker.version_graphics.PokerGame;
import poker.version_graphics.model.Card;
import poker.version_graphics.model.DeckOfCards;
import poker.version_graphics.model.Player;
import poker.version_graphics.model.PokerGameModel;
import poker.version_graphics.view.PlayerPane;
import poker.version_graphics.view.PokerGameView;

public class PokerGameController {
	private PokerGameModel model;
	private PokerGameView view;
	
	public PokerGameController(PokerGameModel model, PokerGameView view) {
		this.model = model;
		this.view = view;
		
		view.getShuffleButton().setOnAction( e -> shuffle() );
		view.getDealButton().setOnAction( e -> deal() );
		for (int i = 0; i < PokerGame.NUM_PLAYERS; i++) {
			PlayerPane pp = view.getPlayerPane(i);
			Button tempBtn =pp.getAddPictureButton();
			Player p = model.getPlayer(i);
			tempBtn.setOnAction(e -> addPicture(p, pp));
		}
	
	
	}

		
	private void addPicture(Player p, PlayerPane pp) {
		 JFileChooser chooser = new JFileChooser();
	        // Dialog zum Oeffnen von Dateien anzeigen
	        int rueckgabeWert = chooser.showOpenDialog(null);
	        
	        /* Abfrage, ob auf "�ffnen" geklickt wurde */
	        if(rueckgabeWert == JFileChooser.APPROVE_OPTION)
	        {
				p.setPicturePath(chooser.getSelectedFile().getAbsolutePath());
				pp.addPicture();
	        }
		
	}

    /**
     * Remove all cards from players hands, and shuffle the deck
     */
    private void shuffle() {
    	for (int i = 0; i < PokerGame.NUM_PLAYERS; i++) {
    		Player p = model.getPlayer(i);
    		p.discardHand();
    		PlayerPane pp = view.getPlayerPane(i);
    		pp.updatePlayerDisplay();
    	}
    	
    	model.getDeck().shuffle();
    }
    
    /**
     * Deal each player five cards, then evaluate the two hands
     */
    private void deal() {
    	int cardsRequired = PokerGame.NUM_PLAYERS * Player.HAND_SIZE;
    	DeckOfCards deck = model.getDeck();
    	if (cardsRequired <= deck.getCardsRemaining()) {
        	for (int i = 0; i < PokerGame.NUM_PLAYERS; i++) {
        		Player p = model.getPlayer(i);
        		p.discardHand();
        		for (int j = 0; j < Player.HAND_SIZE; j++) {
        			Card card = deck.dealCard();
        			p.addCard(card);
        		}
        		p.evaluateHand();
        	
        		PlayerPane pp = view.getPlayerPane(i);
        		pp.ResetWinColor();
        		pp.updatePlayerDisplay();
        	
        		
        	}
        	//Todo Winnermechanismus - Philip Neff
    		WinnerEvaluator we = new WinnerEvaluator();
        	Player winner = we.getWinner(model.getPlayers());
    		winner.setWinCount(winner.getWinCount() + 1);
    		view.getPlayerPane(winner.getPlayerId()).UpdateWinCounter();
        	
        	
    	} else {
            Alert alert = new Alert(AlertType.ERROR, "Not enough cards - shuffle first");
            alert.showAndWait();
    	}
    }
}
