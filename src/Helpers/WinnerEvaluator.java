package Helpers;

import java.util.ArrayList;

import poker.version_graphics.model.Card;
import poker.version_graphics.model.HandType;
import poker.version_graphics.model.Player;

public class WinnerEvaluator {
	//Evaluates the Winner from the Players-Cards - Philip Neff
	public Player getWinner(ArrayList<Player> players) {
		int currentHandType = 0;
		int winnerHandType = -1;
		ArrayList<Card> currentCards= new ArrayList<Card>();
		Player winner = null;
		for(Player p: players) {
			currentCards = p.getCards();
			currentHandType = p.getHandType().ordinal();
			if (currentHandType > winnerHandType) {
				winner = p;
				winnerHandType = currentHandType;//
			}
			else {
				if (currentHandType == winnerHandType) {
					if(HandType.getHighCard(currentCards).getRank().ordinal() > HandType.getHighCard(winner.getCards()).getRank().ordinal()) {
						winner = p;
					}
				}
			}
			winnerHandType = currentHandType;
		}
		return winner;
	}
}
