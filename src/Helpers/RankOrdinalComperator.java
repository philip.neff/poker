package Helpers;

import java.util.Comparator;

import poker.version_graphics.model.Card;

public class RankOrdinalComperator implements Comparator<Card> {
		    @Override
		    public int compare(Card o1, Card o2) {
		        return o1.getRank().ordinal() - o2.getRank().ordinal();
		    }
}
